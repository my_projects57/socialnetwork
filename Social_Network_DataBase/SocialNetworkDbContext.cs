﻿using Microsoft.EntityFrameworkCore;
using Social_Network_Models;

namespace Social_Network_DataBase
{
    public class SocialNetworkDbContext :DbContext
    {
        public SocialNetworkDbContext(DbContextOptions options) : base(options)
        {
        }

        DbSet<User> Users { get; set; }
    }
}
