﻿using Social_Network_Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Social_Network_Repository.Contracts
{
    interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetUserById(int id);
        public void InsertUser(User users);
        public void UpdateUser(User users);
        //IEnumerable<Friends> GetUserFriends(User users);
        Task Save();
        bool IsUserValid(int userId);
        User GetUserByUsername(string username);
    }
}
