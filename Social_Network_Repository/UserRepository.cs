﻿using Social_Network_DataBase;
using Social_Network_Models;
using Social_Network_Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Social_Network_Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly SocialNetworkDbContext _context;

        public UserRepository(SocialNetworkDbContext context)
        {
            _context = context;
        }

        public User GetUserById(int id)
        {
            throw new NotImplementedException();
        }

        public User GetUserByUsername(string username)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public void InsertUser(User users)
        {
            throw new NotImplementedException();
        }

        public bool IsUserValid(int userId)
        {
            throw new NotImplementedException();
        }

        public Task Save()
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(User users)
        {
            throw new NotImplementedException();
        }
    }
}
